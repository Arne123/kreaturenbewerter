% probability distribution of the sum of n w-sided die
function prob = probabilityDistributionMultipleDice(w,n)
prob = [];
    for m = n : n*w
        prob(m) = 0;
        for k = 0:w
            j = -w*k + +m - n;
            if j >= 0
                prob(m) = prob(m) + binomialCoefficient(n,k)*binomialCoefficient(-n,j)* ((-1)^(k+j) / (w^n));
            end
        end
    end
end