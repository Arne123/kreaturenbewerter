% binomial coefficient for negative n
function binom = binomialCoefficient(n,k)
    if n>0
        binom = nchoosek(n,k);
    else
        binom = nchoosek(-n+k-1,k)*(-1)^k;
    end
end