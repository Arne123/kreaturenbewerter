clear all
% Alrik
Alrik.AT = 12;
Alrik.VT = 12;
Alrik.TP.W = 6;
Alrik.TP.Anzahl = 3;
Alrik.TP.Plus = 5;
Alrik.WS = 6;
Alrik.Koloss = 0;
Alrik.NoAttacks = 1;

% Kreatur
Kreatur.AT = 12;
Kreatur.VT = 12;
Kreatur.TP.W = 6;
Kreatur.TP.Anzahl = 4;
Kreatur.TP.Plus = 6;
Kreatur.WS = 10;
Kreatur.Koloss = 2;
Kreatur.NoAttacks = 1;

% Calculate wounds per round that Alrik inflicts on Kreatur
AlrikProbToHit = 0; % probability for Alrik to hit Kreatur
for i = -19:19
    if Alrik.AT+i >= Kreatur.VT
        AlrikProbToHit = AlrikProbToHit + (20- abs(i))/400;
    end
end

% calculate wounds per hit of Alrik on Kreatur
AlrikDamageOutput = 0;
DistributionTP = probabilityDistributionMultipleDice(Alrik.TP.W, Alrik.TP.Anzahl);
for i = 1:length(DistributionTP)
    AlrikDamageOutput = AlrikDamageOutput + DistributionTP(i)*floor((i+Alrik.TP.Plus)/Kreatur.WS)/(2^Kreatur.Koloss);
end
AlrikDamageOutput = AlrikProbToHit * AlrikDamageOutput * Alrik.NoAttacks;
%AlrikProbToHit
AlrikDamageOutput

% now vice-versa
% Calculate wounds per round that Kreatur inflicts on Alrik
for a = 1:Kreatur.NoAttacks
    KreaturProbToHit(a) = 0; % probability for Alrik to hit Kreatur
    for i = -19:19
        if Kreatur.AT+i >= Alrik.VT-(a-1)*4;
            KreaturProbToHit(a) = KreaturProbToHit(a) + (20- abs(i))/400;
        end
    end
end

% calculate wounds per hit of Alrik on Kreatur
KreaturDamageOutput = 0;
DistributionTP = probabilityDistributionMultipleDice(Kreatur.TP.W, Kreatur.TP.Anzahl);
for i = 1:length(DistributionTP)
    KreaturDamageOutput = KreaturDamageOutput + DistributionTP(i)*floor((i+Kreatur.TP.Plus)/Alrik.WS)/(2^Alrik.Koloss);
end
KreaturDamageOutput = KreaturDamageOutput * sum(KreaturProbToHit);
%KreaturProbToHit
KreaturDamageOutput

MeleeProwess = KreaturDamageOutput/AlrikDamageOutput;
MeleeProwess